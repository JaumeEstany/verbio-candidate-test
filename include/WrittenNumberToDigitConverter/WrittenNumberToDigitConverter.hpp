
#pragma once

#include <string>
#include <vector>
#include <memory>

#include "WrittenNumberToDigitConverter/TextToKeywordNodeReplacer.hpp"
#include "WrittenNumberToDigitConverter/KeywordNodeToTextNodeReplacer.hpp"
#include "WrittenNumberToDigitConverter/WordSplitter.hpp"
#include "WrittenNumberToDigitConverter/Nodes/Node.hpp"

class WrittenNumberToDigitConverter
{
    public:
        std::string transform(const std::string &target);

    private:
        void searchForKeywordNodes();
        void joiningKeywordNodes();

        bool isSoftSeparatorNode(Node* nodePtr);
        bool isKeywordNode(Node* nodePtr);

    private:
        WordSplitter wordSplitter;
        TextToKeywordNodeReplacer textToKeywordNodeReplacer;
        KeywordNodeToTextNodeReplacer keywordNodeToTextNodeReplacer;
        std::vector<std::unique_ptr<Node>> nodes;
};
