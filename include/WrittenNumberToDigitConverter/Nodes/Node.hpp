
#pragma once

#include <string>

class Node
{
    public:
        virtual std::string getAsString() const = 0;
};
