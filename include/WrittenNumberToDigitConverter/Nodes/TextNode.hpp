
#pragma once

#include <string>

#include "WrittenNumberToDigitConverter/Nodes/Node.hpp"

class TextNode: public Node
{
    public:
        TextNode(std::string text = "");

        std::string getAsString() const;
        void setText(std::string newText);

    protected:
        std::string text;
};
