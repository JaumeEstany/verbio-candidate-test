
#pragma once

#include <string>

#include "WrittenNumberToDigitConverter/Nodes/TextNode.hpp"

class SeparatorNode: public TextNode
{
    public:
        bool isSoftSeparator() const;
    private:
        bool isOnlySpaces() const;
        bool charIsSpace(char target) const;
};
