
#pragma once

#include <string>
#include <memory>

#include "WrittenNumberToDigitConverter/Nodes/Node.hpp"

class Builder
{
    public:
        std::unique_ptr<Node> buildByKeyword(std::string keyword) const;

    private:
        std::string loadedString;
};
