
#pragma once

#include <string>
#include <map>
#include <memory>

#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/KeywordNode.hpp"
#include "WrittenNumberToDigitConverter/Nodes/Node.hpp"

/*
 * Node that contains one unit component word:
 * one, two, three, ...
 */
class UnitComponentNode: public KeywordNode
{
    public:
        virtual KeywordNode::KeywordType getKeywordType() const;
        static std::unique_ptr<Node> buildFromKeyword(const std::string &keyword);

    private:
        UnitComponentNode(std::string originalString, int numberValue);

    private:
        const static std::map<std::string, int> numberUnitComponents;

        int numberValue;
};
