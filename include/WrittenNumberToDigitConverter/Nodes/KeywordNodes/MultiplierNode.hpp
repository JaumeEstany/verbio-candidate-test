
#pragma once

#include <string>
#include <map>
#include <memory>

#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/KeywordNode.hpp"
#include "WrittenNumberToDigitConverter/Nodes/Node.hpp"

/*
 * Node that contains one word, and that word is a multiplier of previous numbers:
 * hundred, thousand, ...
 */
class MultiplierNode: public KeywordNode
{
    public:
        virtual KeywordNode::KeywordType getKeywordType() const override;
        static std::unique_ptr<Node> buildFromKeyword(const std::string &keyword);

    private:
        MultiplierNode(const std::string &originalString, int numberValue);

    private:
        const static std::map<std::string, int> numberMultipliers;

        int numberValue;
};
