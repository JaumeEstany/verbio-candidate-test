
#pragma once

#include <string>
#include <map>
#include <memory>

#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/KeywordNode.hpp"
#include "WrittenNumberToDigitConverter/Nodes/Node.hpp"

/*
 * Node that contains one word, and that word means an all-defined number:
 * eleven, twelve, ...
 */
class SimpleNumberNode: public KeywordNode
{
    public:
        virtual KeywordNode::KeywordType getKeywordType() const;
        static std::unique_ptr<Node> buildFromKeyword(const std::string &keyword);

    private:
        SimpleNumberNode(std::string originalString, int numberValue);

    private:
        const static std::map<std::string, int> simpleNumbers;

        int numberValue;
};
