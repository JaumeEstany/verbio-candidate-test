
#pragma once

#include <string>

#include "WrittenNumberToDigitConverter/Nodes/Node.hpp"

class KeywordNode: public Node
{
    public:
        enum KeywordType {
            SimpleNumberNode,
            UnitComponentNode,
            TensComponentNode,
            MultiplierNode};

        virtual KeywordType getKeywordType() const = 0;

        virtual std::string getAsString() const override;

    protected:
        std::string originalString;
};
