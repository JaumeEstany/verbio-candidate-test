
#pragma once

#include <string>
#include <vector>
#include <memory>

#include "WrittenNumberToDigitConverter/Nodes/Node.hpp"
#include "WrittenNumberToDigitConverter/Nodes/TextNode.hpp"
#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/KeywordNode.hpp"
#include "WrittenNumberToDigitConverter/Nodes/SeparatorNode.hpp"

typedef std::vector<std::unique_ptr<Node>> NodeList;
typedef std::vector<KeywordNode*> KeywordNodeList;

class KeywordNodeToTextNodeReplacer
{
    public:
        void setSourceVector(NodeList &source);
        void tryTransformAndPushNodeRange(NodeList &output, int indexStart, int indexEnd);

        bool isSoftSeparatorNode(Node* nodePtr);
        bool isKeywordNode(Node* nodePtr);
        KeywordNode* getAsKeywordNode(Node* nodePtr);

    private:
        KeywordNodeList&& getRangeAndFilterSeparators(int indexStart, int indexEnd);
        std::unique_ptr<TextNode>&& getTextNodeByKeywordNodeRange(const KeywordNodeList &keywordNodes);
        void pushNodeRangeToOutput(NodeList &output, int indexStart, int indexEnd);
    private:
        NodeList* sourceVector;
};
