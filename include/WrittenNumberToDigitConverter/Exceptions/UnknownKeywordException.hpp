
#pragma once

#include <exception>

#include "Base/Exceptions/VerboseException.hpp"

class UnknownKeywordException : public VerboseException
{
    public:
    UnknownKeywordException(std::string message = ""): VerboseException(message) {}
};
