
#pragma once

#include <string>
#include <memory>

#include "WrittenNumberToDigitConverter/Nodes/Node.hpp"
#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/Builder.hpp"

class TextToKeywordNodeReplacer
{
    public:
        void setString(const std::string &targetString);
        std::unique_ptr<Node> getCorrespondingNode() const;

    private:
        Builder nodeBuilder;
        std::string loadedString;
};
