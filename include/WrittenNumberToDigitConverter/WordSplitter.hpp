
#pragma once

#include <string>
#include <vector>
#include <memory>

#include "WrittenNumberToDigitConverter/Nodes/Node.hpp"

class WordSplitter
{
    public:
        void split(const std::string &target);
        const Node& getNode(const int index) const;
        int getNumNodes() const;

    private:
        void resetObject();
        void splitAndSaveNodes(const std::string &target);

        bool isSeparatorChar(char target);

    private:
        std::vector<std::unique_ptr<Node>> nodes;
};
