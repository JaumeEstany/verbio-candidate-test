
#pragma once

#include <string>
#include <exception>

class VerboseException : public std::exception
{
    public:
        VerboseException(std::string message = "")
        {
            this->message = message;
        }

        virtual const char* what() const throw() {
            return message.c_str();
        }
    private:
        std::string message;
};
