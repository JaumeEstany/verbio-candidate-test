
#pragma once

#include <string>
#include <map>

class String
{
    public:
        static std::string toLowercase(std::string target);
        static std::string toUppercase(std::string target);
};
