
#include <myTestingLib/LibTestClass.h>

#include "WrittenNumberToDigitConverter/WordSplitter.h"

class WordSplitterTest : public LibTestClass
{
    public:
        void regularSplitCase()
        {
            WordSplitter wordSplitter;
            wordSplitter.split("The quick brown fox");

            libAssertEqual(wordSplitter.getNumNodes(), 7);
            libAssertEqual(wordSplitter.getNode(0).getAsString(), "The");
            libAssertEqual(wordSplitter.getNode(1).getAsString(), " ");
            libAssertEqual(wordSplitter.getNode(2).getAsString(), "quick");
            libAssertEqual(wordSplitter.getNode(3).getAsString(), " ");
            libAssertEqual(wordSplitter.getNode(4).getAsString(), "brown");
            libAssertEqual(wordSplitter.getNode(5).getAsString(), " ");
            libAssertEqual(wordSplitter.getNode(6).getAsString(), "fox");
        }

        void startingEndingSpacesCase()
        {
            WordSplitter wordSplitter;
            wordSplitter.split("    The quick brown fox    ");

            libAssertEqual(wordSplitter.getNumNodes(), 7);
            libAssertEqual(wordSplitter.getNode(0).getAsString(), "    ");
            libAssertEqual(wordSplitter.getNode(1).getAsString(), "The");
            libAssertEqual(wordSplitter.getNode(2).getAsString(), " ");
            libAssertEqual(wordSplitter.getNode(3).getAsString(), "quick");
            libAssertEqual(wordSplitter.getNode(4).getAsString(), " ");
            libAssertEqual(wordSplitter.getNode(5).getAsString(), "brown");
            libAssertEqual(wordSplitter.getNode(6).getAsString(), " ");
            libAssertEqual(wordSplitter.getNode(7).getAsString(), "fox");
            libAssertEqual(wordSplitter.getNode(7).getAsString(), "    ");
        }

        void specialSeparatorsCase()
        {
            WordSplitter wordSplitter;
            wordSplitter.split("The.quick-brown+fox");

            libAssertEqual(wordSplitter.getNumNodes(), 7);
            libAssertEqual(wordSplitter.getNode(0).getAsString(), "The");
            libAssertEqual(wordSplitter.getNode(1).getAsString(), ".");
            libAssertEqual(wordSplitter.getNode(2).getAsString(), "quick");
            libAssertEqual(wordSplitter.getNode(3).getAsString(), "-");
            libAssertEqual(wordSplitter.getNode(4).getAsString(), "brown");
            libAssertEqual(wordSplitter.getNode(5).getAsString(), "/");
            libAssertEqual(wordSplitter.getNode(6).getAsString(), "fox");
        }

        void startingEndingSpecialSeparatorsCase()
        {
            WordSplitter wordSplitter;
            wordSplitter.split("-&./The-&./quick-&./brown-&./fox-&./");

            libAssertEqual(wordSplitter.getNumNodes(), 7);
            libAssertEqual(wordSplitter.getNode(0).getAsString(), "-&./");
            libAssertEqual(wordSplitter.getNode(1).getAsString(), "The");
            libAssertEqual(wordSplitter.getNode(2).getAsString(), "-&./");
            libAssertEqual(wordSplitter.getNode(3).getAsString(), "quick");
            libAssertEqual(wordSplitter.getNode(4).getAsString(), "-&./");
            libAssertEqual(wordSplitter.getNode(5).getAsString(), "brown");
            libAssertEqual(wordSplitter.getNode(6).getAsString(), "-&./");
            libAssertEqual(wordSplitter.getNode(7).getAsString(), "fox");
            libAssertEqual(wordSplitter.getNode(7).getAsString(), "-&./");
        }
};
