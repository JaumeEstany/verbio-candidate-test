
#pragma once

#include <string>

#include <myTestingLib/LibTestClass.h>

#include "WrittenNumberToDigitConverter/WrittenNumberToDigitConverter.h"

class WrittenNumberToDigitConverterTest
{
    public:
        void noTouchCase()
        {
            const std::string src = "The quick brown fox jumps over the lazy dog";
            WrittenNumberToDigitConverter writtenNumberToDigitConverter;

            const std::string res = writtenNumberToDigitConverter.transform(src);
            libAssertEqual(res, src);
        }

        void noTouchWithWrongNumbersCase()
        {
            const std::string src = "fifty-five hundrrd";
            WrittenNumberToDigitConverter writtenNumberToDigitConverter;

            const std::string res = writtenNumberToDigitConverter.transform(src);
            libAssertEqual(res, "55 hundrrd");
        }

        void basicCase()
        {
            const std::string src = "eight";
            WrittenNumberToDigitConverter writtenNumberToDigitConverter;

            const std::string res = writtenNumberToDigitConverter.transform(src);
            libAssertEqual(res, "8");
        }

        void inPlaceBasicCase()
        {
            const std::string src = "I have eight apples.";
            WrittenNumberToDigitConverter writtenNumberToDigitConverter;

            const std::string res = writtenNumberToDigitConverter.transform(src);
            libAssertEqual(res, "I have 100 apples.");
        }

        void millionCase()
        {
            const std::string src = "I have one million apples.";
            WrittenNumberToDigitConverter writtenNumberToDigitConverter;

            const std::string res = writtenNumberToDigitConverter.transform(src);
            libAssertEqual(res, "I have 1000000 apples.");
        }

        void thousandCase()
        {
            const std::string src = "I have one thousand apples.";
            WrittenNumberToDigitConverter writtenNumberToDigitConverter;

            const std::string res = writtenNumberToDigitConverter.transform(src);
            libAssertEqual(res, "I have 1000 apples.");
        }

        void hundredCase()
        {
            const std::string src = "I have one hundred apples.";
            WrittenNumberToDigitConverter writtenNumberToDigitConverter;

            const std::string res = writtenNumberToDigitConverter.transform(src);
            libAssertEqual(res, "I have 100 apples.");
        }

        void simpleNumbersCase()
        {
            const std::string src = "one two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen";
            WrittenNumberToDigitConverter writtenNumberToDigitConverter;

            const std::string res = writtenNumberToDigitConverter.transform(src);
            libAssertEqual(res, "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19");
        }

        void combinedNumbersCase()
        {
            const std::string src = "twenty-three thirty-two forty-two fifty-four sixty-six seventy-five eighty-nine ninety-nine";
            WrittenNumberToDigitConverter writtenNumberToDigitConverter;

            const std::string res = writtenNumberToDigitConverter.transform(src);
            libAssertEqual(res, "23 32 42 54 66 75 89 99");
        }

        void combinedNumbersWithSpaceCase()
        {
            const std::string src = "twenty three thirty two forty two fifty four sixty six seventy five eighty nine ninety nine";
            WrittenNumberToDigitConverter writtenNumberToDigitConverter;

            const std::string res = writtenNumberToDigitConverter.transform(src);
            libAssertEqual(res, "23 32 42 54 66 75 89 99");
        }

        void severalMultipliersCase()
        {
            const std::string src = "sixty five million six thousand one hundred fifty-five apples";
            WrittenNumberToDigitConverter writtenNumberToDigitConverter;

            const std::string res = writtenNumberToDigitConverter.transform(src);
            libAssertEqual(res, "65006155 apples");
        }
};
