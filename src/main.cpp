
#include <iostream>
#include <string>

#include "WrittenNumberToDigitConverter/WrittenNumberToDigitConverter.hpp"



int main ()
{
    std::string src = "";
    src = "The dynamic_cast one hundred. C++ pointer casts: cast";
    src = "two hundred apples";

    WrittenNumberToDigitConverter converter;
    std::string res = converter.transform(src);

    std::cout << "----------------------------------------" << std::endl;
    std::cout << '"' << res << '"' << std::endl;
    return 0;
}


