
#include "Utils/String.hpp"

#include <string>
#include <algorithm>


std::string String::toLowercase(std::string target)
{
    std::transform(target.begin(), target.end(), target.begin(), ::tolower);
    return target;
}

std::string String::toUppercase(std::string target)
{
    std::transform(target.begin(), target.end(), target.begin(), ::toupper);
    return target;
}
