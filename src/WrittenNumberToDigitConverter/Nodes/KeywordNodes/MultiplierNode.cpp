
#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/MultiplierNode.hpp"
#include "WrittenNumberToDigitConverter/Exceptions/UnknownKeywordException.hpp"

#include <iostream>

#include "Utils/String.hpp"

const std::map<std::string, int> MultiplierNode::numberMultipliers = {
    {"hundred", 100},
    {"thousand", 1000},
    {"million", 1000000}
};

KeywordNode::KeywordType MultiplierNode::getKeywordType() const
{
    return KeywordNode::KeywordType::MultiplierNode;
}

MultiplierNode::MultiplierNode(const std::string &originalString, int numberValue): KeywordNode()
{
    this->originalString = originalString;
    this->numberValue = numberValue;
}

std::unique_ptr<Node> MultiplierNode::buildFromKeyword(const std::string &keyword)
{
    const std::string lowercaseKeyword = String::toLowercase(keyword);

    auto keywordMapEntry = numberMultipliers.find(lowercaseKeyword);
    const bool isKeywordMissing = (keywordMapEntry == numberMultipliers.end());
    if (isKeywordMissing) {
        return std::unique_ptr<Node>();
    }

    const int associatedNumber = keywordMapEntry->second;

    MultiplierNode *multiplierNode = new MultiplierNode(keyword, associatedNumber);
    Node *node = dynamic_cast<Node*>(multiplierNode);

    std::unique_ptr<Node> ret(node);
    return std::move(ret);
}
