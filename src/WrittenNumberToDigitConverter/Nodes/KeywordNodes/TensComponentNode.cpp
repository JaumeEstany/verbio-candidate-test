
#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/TensComponentNode.hpp"
#include "WrittenNumberToDigitConverter/Exceptions/UnknownKeywordException.hpp"

#include "Utils/String.hpp"

const std::map<std::string, int> TensComponentNode::numberTensComponents = {
    {"twenty", 20},
    {"thirty", 30},
    {"forty", 40},
    {"fifty", 50},
    {"sixty", 60},
    {"seventy", 70},
    {"eighty", 80},
    {"ninety", 90}
};

KeywordNode::KeywordType TensComponentNode::getKeywordType() const
{
    return KeywordNode::KeywordType::TensComponentNode;
}

TensComponentNode::TensComponentNode(std::string originalString, int numberValue)
{
    this->originalString = originalString;
    this->numberValue = numberValue;
}

std::unique_ptr<Node> TensComponentNode::buildFromKeyword(const std::string &keyword)
{
    const std::string lowercaseKeyword = String::toLowercase(keyword);

    auto keywordMapEntry = numberTensComponents.find(lowercaseKeyword);
    const bool isKeywordMissing = (keywordMapEntry == numberTensComponents.end());
    if (isKeywordMissing) {
        return std::unique_ptr<Node>();
    }

    const int associatedNumber = keywordMapEntry->second;

    TensComponentNode *tensComponentNode = new TensComponentNode(keyword, associatedNumber);
    Node *node = dynamic_cast<Node*>(tensComponentNode);

    std::unique_ptr<Node> ret(node);
    return std::move(ret);
}
