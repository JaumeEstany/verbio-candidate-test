
#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/Builder.hpp"
#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/SimpleNumberNode.hpp"
#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/UnitComponentNode.hpp"
#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/TensComponentNode.hpp"
#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/MultiplierNode.hpp"

std::unique_ptr<Node> Builder::buildByKeyword(std::string keyword) const
{
    std::unique_ptr<Node> &&simpleNumberNode = SimpleNumberNode::buildFromKeyword(keyword);
    if (simpleNumberNode.get() != nullptr) {
        return std::move(simpleNumberNode);
    }

    std::unique_ptr<Node> &&unitComponentNode = UnitComponentNode::buildFromKeyword(keyword);
    if (unitComponentNode.get() != nullptr) {
        return std::move(unitComponentNode);
    }

    std::unique_ptr<Node> &&tensComponentNode = TensComponentNode::buildFromKeyword(keyword);
    if (tensComponentNode.get() != nullptr) {
        return std::move(tensComponentNode);
    }

    std::unique_ptr<Node> &&multiplierNode = MultiplierNode::buildFromKeyword(keyword);
    if (multiplierNode.get() != nullptr) {
        return std::move(multiplierNode);
    }

    return std::unique_ptr<Node>();
}

