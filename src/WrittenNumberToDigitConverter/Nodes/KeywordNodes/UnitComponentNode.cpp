
#include <iostream>

#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/UnitComponentNode.hpp"
#include "WrittenNumberToDigitConverter/Exceptions/UnknownKeywordException.hpp"

#include "Utils/String.hpp"

const std::map<std::string, int> UnitComponentNode::numberUnitComponents = {
    {"one", 1},
    {"two", 2},
    {"three", 3},
    {"four", 4},
    {"five", 5},
    {"six", 6},
    {"seven", 7},
    {"eight", 8},
    {"nine", 9}
};

KeywordNode::KeywordType UnitComponentNode::getKeywordType() const
{
    return KeywordNode::KeywordType::UnitComponentNode;
}

UnitComponentNode::UnitComponentNode(std::string originalString, int numberValue)
{
    this->originalString = originalString;
    this->numberValue = numberValue;
}

std::unique_ptr<Node> UnitComponentNode::buildFromKeyword(const std::string &keyword)
{
    const std::string lowercaseKeyword = String::toLowercase(keyword);

    auto keywordMapEntry = numberUnitComponents.find(lowercaseKeyword);
    const bool isKeywordMissing = (keywordMapEntry == numberUnitComponents.end());
    if (isKeywordMissing) {
        return std::unique_ptr<Node>();
    }

    const int associatedNumber = keywordMapEntry->second;

    UnitComponentNode *unitComponentNode = new UnitComponentNode(keyword, associatedNumber);
    Node *node = dynamic_cast<Node*>(unitComponentNode);

    std::unique_ptr<Node> ret(node);
    return std::move(ret);
}
