
#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/SimpleNumberNode.hpp"
#include "WrittenNumberToDigitConverter/Exceptions/UnknownKeywordException.hpp"

#include "Utils/String.hpp"

const std::map<std::string, int> SimpleNumberNode::simpleNumbers = {
    {"ten", 10},
    {"eleven", 11},
    {"twelve", 12},
    {"thirteen", 13},
    {"fourteen", 14},
    {"fifteen", 15},
    {"sixteen", 16},
    {"seventeen", 17},
    {"eighteen", 18},
    {"nineteen", 19}
};

KeywordNode::KeywordType SimpleNumberNode::getKeywordType() const
{
    return KeywordNode::KeywordType::SimpleNumberNode;
}

SimpleNumberNode::SimpleNumberNode(std::string originalString, int numberValue)
{
    this->originalString = originalString;
    this->numberValue = numberValue;
}

std::unique_ptr<Node> SimpleNumberNode::buildFromKeyword(const std::string &keyword)
{
    const std::string lowercaseKeyword = String::toLowercase(keyword);

    auto keywordMapEntry = simpleNumbers.find(lowercaseKeyword);
    const bool isKeywordMissing = (keywordMapEntry == simpleNumbers.end());
    if (isKeywordMissing) {
        return std::unique_ptr<Node>();
    }

    const int associatedNumber = keywordMapEntry->second;

    SimpleNumberNode *simpleNumberNode = new SimpleNumberNode(keyword, associatedNumber);
    Node *node = dynamic_cast<Node*>(simpleNumberNode);

    std::unique_ptr<Node> ret(node);
    return std::move(ret);
}
