
#include "WrittenNumberToDigitConverter/Nodes/SeparatorNode.hpp"

#include <iostream>
#include <string>

bool SeparatorNode::isSoftSeparator() const
{
    return (text == "-" || text == "" || isOnlySpaces());
}

bool SeparatorNode::isOnlySpaces() const
{
    for (const char c : text) {
        if (!charIsSpace(c)) {
            return false;
        }
    }

    return true;
}

bool SeparatorNode::charIsSpace(char target) const
{
    return (target == ' ');
}
