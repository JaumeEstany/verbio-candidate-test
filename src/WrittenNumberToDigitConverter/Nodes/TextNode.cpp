
#include "WrittenNumberToDigitConverter/Nodes/TextNode.hpp"

#include <iostream>
#include <string>

TextNode::TextNode(std::string text)
{
    this->text = text;
}
std::string TextNode::getAsString() const
{
    return text;
}

void TextNode::setText(std::string newText)
{
    text = newText;
}