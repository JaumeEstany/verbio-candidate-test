
#include "WrittenNumberToDigitConverter/TextToKeywordNodeReplacer.hpp"

#include <iostream>
#include <map>

#include "WrittenNumberToDigitConverter/Exceptions/UnknownKeywordException.hpp"
#include "WrittenNumberToDigitConverter/Nodes/TextNode.hpp"

void TextToKeywordNodeReplacer::setString(const std::string &targetString)
{
    loadedString = targetString;
}

std::unique_ptr<Node> TextToKeywordNodeReplacer::getCorrespondingNode() const
{
    std::unique_ptr<Node> builtNode = nodeBuilder.buildByKeyword(loadedString);

    if (builtNode.get() == nullptr) {
        std::unique_ptr<Node> ret(new TextNode(loadedString));
        return std::move(ret);
    }

    return std::move(builtNode);
}
