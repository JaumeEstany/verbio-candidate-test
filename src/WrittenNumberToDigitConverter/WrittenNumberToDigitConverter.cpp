
#include "WrittenNumberToDigitConverter/WrittenNumberToDigitConverter.hpp"

#include <iostream>

#include "WrittenNumberToDigitConverter/Nodes/SeparatorNode.hpp"
#include "WrittenNumberToDigitConverter/Nodes/KeywordNodes/KeywordNode.hpp"


std::string WrittenNumberToDigitConverter::transform(const std::string &target)
{
    nodes.clear();

    wordSplitter.split(target);
    searchForKeywordNodes();
    joiningKeywordNodes();

    return target;
}

void WrittenNumberToDigitConverter::searchForKeywordNodes()
{
    for (int i = 0; i < wordSplitter.getNumNodes(); i++) {
        const Node &textNode = wordSplitter.getNode(i);

        const std::string nodeOriginalString = textNode.getAsString();
        textToKeywordNodeReplacer.setString(nodeOriginalString);

        std::unique_ptr<Node> newNode = textToKeywordNodeReplacer.getCorrespondingNode();
        nodes.push_back(std::move(newNode));
    }
}


void WrittenNumberToDigitConverter::joiningKeywordNodes()
{
    keywordNodeToTextNodeReplacer.setSourceVector(nodes);
    std::vector<std::unique_ptr<Node>> newNodeList;

    int i = 0;
    const int numNodes = nodes.size();
    while (i < numNodes) {
        const int groupStartingIndex = i;

        if (isKeywordNode(nodes[i].get())) {
            while (i < numNodes && (isKeywordNode(nodes[i].get()) || isSoftSeparatorNode(nodes[i].get()))) {
                i++;
            }

            const int groupEndingIndex = i;
            keywordNodeToTextNodeReplacer.tryTransformAndPushNodeRange(
                newNodeList, groupStartingIndex, groupEndingIndex);
        } else {
            newNodeList.push_back(std::move(nodes[i]));
            i++;
        }
    }

}

bool WrittenNumberToDigitConverter::isSoftSeparatorNode(Node* nodePtr)
{
    return keywordNodeToTextNodeReplacer.isSoftSeparatorNode(nodePtr);
}

bool WrittenNumberToDigitConverter::isKeywordNode(Node* nodePtr)
{
    return keywordNodeToTextNodeReplacer.isKeywordNode(nodePtr);
}
