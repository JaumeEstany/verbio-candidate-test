
#include "WrittenNumberToDigitConverter/KeywordNodeToTextNodeReplacer.hpp"

#include <algorithm>

void KeywordNodeToTextNodeReplacer::setSourceVector(NodeList &source)
{
    this->sourceVector = &source;
}

void KeywordNodeToTextNodeReplacer::tryTransformAndPushNodeRange(NodeList& output, const int indexStart, const int indexEnd)
{

    const KeywordNodeList keywordNodes = getRangeAndFilterSeparators(indexStart, indexEnd);
    std::unique_ptr<TextNode> generatedTextNode = getTextNodeByKeywordNodeRange(keywordNodes);

    const bool numberWasTransformed = (generatedTextNode.get() != nullptr);
    if (numberWasTransformed) {
        output.push_back(std::move(generatedTextNode));
    } else {
        pushNodeRangeToOutput(output, indexStart, indexEnd);
    }
    
}

KeywordNodeList&& KeywordNodeToTextNodeReplacer::getRangeAndFilterSeparators(int indexStart, int indexEnd)
{
    NodeList &nodeSource = (*this->sourceVector);
    const int nodeListLength = (nodeSource.size());

    KeywordNodeList keywordNodes;

    indexStart = std::max(indexStart, 0);
    indexEnd = std::min(indexEnd, nodeListLength);

    for (int i = indexStart; i < indexEnd ; i++) {
        Node* node = nodeSource[i].get();
        KeywordNode* keywordNode = getAsKeywordNode(node);

        if (keywordNode != nullptr) {
            keywordNodes.push_back(keywordNode);
        }
    }

    return std::move(keywordNodes);
}

std::unique_ptr<TextNode>&& KeywordNodeToTextNodeReplacer::getTextNodeByKeywordNodeRange(const KeywordNodeList &keywordNodes)
{
    /*
    Check if the pattern indicates that this is a written number

    Numbers that
    <SimpleNumber> -> eleven, twelve, ...
    <UnitComponent> -> one, two, three ...
    <TensComponent> -> twenty, thirty, ...
    <MultiplierNumber> -> hundred, thousand ...

    By order:
    try to match million quantity
    (<UnitComponent><MultiplierNumber(million)>)?

    try to match thousand quantity
    (<UnitComponent><MultiplierNumber(thousand)>)?

    try to match hundred quantity
    (<UnitComponent><MultiplierNumber(hundred)>)?

    try to match tens and unit quantity
    (<SimpleNumber>|(<TensComponent>)?(<UnitComponent>)?)?

    Match at least one of the rows above by order.
    If that is the case, calculate the written number using the internal values each node has.

    If the correct match does not happen, return null pointer (as the code below does).
    This will forget about possible keywords and restore the original text.
    */

    return std::move(std::unique_ptr<TextNode>());
}

void KeywordNodeToTextNodeReplacer::pushNodeRangeToOutput(NodeList &output, int indexStart, int indexEnd)
{
    NodeList &nodeSource = (*this->sourceVector);
    const int nodeListLength = (nodeSource.size());

    indexStart = std::max(indexStart, 0);
    indexEnd = std::min(indexEnd, nodeListLength);

    for (int i = indexStart; i < indexEnd ; i++) {
        output.push_back(std::move(nodeSource[i]));
    }
}

bool KeywordNodeToTextNodeReplacer::isSoftSeparatorNode(Node* nodePtr)
{
    SeparatorNode* casted = dynamic_cast<SeparatorNode*>(nodePtr);
    if (casted == nullptr) {
        return false;
    }

    return casted->isSoftSeparator();
}

bool KeywordNodeToTextNodeReplacer::isKeywordNode(Node* nodePtr)
{
    KeywordNode* casted = getAsKeywordNode(nodePtr);
    return (casted != nullptr);
}

KeywordNode* KeywordNodeToTextNodeReplacer::getAsKeywordNode(Node* nodePtr)
{
    KeywordNode* casted = dynamic_cast<KeywordNode*>(nodePtr);
    return casted;
}

