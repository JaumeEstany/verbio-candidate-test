
#include "WrittenNumberToDigitConverter/WordSplitter.hpp"

#include <iostream>
#include <string>
#include <cstring>

#include "WrittenNumberToDigitConverter/Nodes/TextNode.hpp"
#include "WrittenNumberToDigitConverter/Nodes/WordNode.hpp"
#include "WrittenNumberToDigitConverter/Nodes/SeparatorNode.hpp"

void WordSplitter::split(const std::string &target)
{
    resetObject();
    splitAndSaveNodes(target);
}

const Node& WordSplitter::getNode(const int index) const
{
    return (*(nodes[index]));
}

int WordSplitter::getNumNodes() const
{
    return nodes.size();
}

void WordSplitter::resetObject()
{
    nodes.clear();
}

void WordSplitter::splitAndSaveNodes(const std::string &target)
{
    int i = 0;
    const int targetStringSize = target.size();

    while (i < targetStringSize) {
        const bool isSeparatorNode = isSeparatorChar(target[i]);

        std::unique_ptr<TextNode> currentNode;
        if (isSeparatorNode) {
            currentNode.reset(new SeparatorNode());
        } else {
            currentNode.reset(new WordNode());
        }

        const int substringStartIndex = i;
        if (isSeparatorNode) {
            while (i < targetStringSize && isSeparatorChar(target[i])) {
                i++;
            }
        } else {
            while (i < targetStringSize && !isSeparatorChar(target[i])) {
                i++;
            }
        }

        const int substringLength = (i - substringStartIndex);

        std::string nodeText = target.substr(substringStartIndex, substringLength);

        currentNode->setText(nodeText);
        this->nodes.push_back(std::move(currentNode));
    }
}

bool WordSplitter::isSeparatorChar(char target)
{
    bool isAnAlphabetCharacter = isalpha(target);
    return !isAnAlphabetCharacter;
}
